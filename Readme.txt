URL Shortcut Viewer plugin 1.1.1.0 for Total Commander
======================================================

 * License:
-----------

This software is released as freeware.


 * Disclaimer:
--------------

This software is provided "AS IS" without any warranty, either expressed or
implied, including, but not limited to, the implied warranties of
merchantability and fitness for a particular purpose. The author will not be
liable for any special, incidental, consequential or indirect damages due to
loss of data or any other reason.


 * Installation:
----------------

 1. Unzip the archive to an empty directory
 2. In Total Commander choose "Configuration => Options"
 3. Choose "Edit/View"
 4. Click "Configure internal viewer..."
 5. Click "LS-Plugins"
 6. Click "Add" and select the urlview.wlx


 * Description:
---------------

The URL Shortcut Viewer plugin uses the Internet Explorer browser component to
display the website of URL files (Internet shortcuts) in the lister window of
Total Commander.

Internet Explorer 5.0 is at least required.

Warning: Do not use this plugin for viewing files of unknown origin that might
contain malicious content.


 * Examples:
------------

 o example.url refers to my website offering miscellaneous Freeware for download.


 * Known problems:
------------------

 o Viewing files in Quick View mode loses the focus from the other panel.
 o The standard keys (N, P, CTRL+A, CTRL+C, CTRL+P, ESC, ...) of lister do not
   work. However, you can always close the lister window by ALT+F4.


 * ChangeLog:
-------------

 o Version 1.1.1.0 (28.10.2011)
   - removed MPRESS binary compression
 o Version 1.1.1.0 (29.09.2011)
   - added support of Unicode file names
   - added 64 bit support
 o Version 1.1.0.0 (28.11.2006)
   - added: avoid flickering of preview window when switching from one file to
     the next (for Total Commander >= 7 beta 2)
 o Version 1.0.0.1 (08.12.2005)
   - first public version


 * References:
--------------

 o LS-Plugin Writer's Guide by Christian Ghisler
   - http://ghisler.fileburst.com/lsplugins/listplughelp2.1.zip


 * Trademark and Copyright Statements:
--------------------------------------

 o Total Commander is Copyright � 1993-2011 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net